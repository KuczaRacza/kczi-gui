#version 450

layout(location=0)in vec2 in_Position;
layout(location=1)in vec2 in_TexCoord;

uniform mat4 u_mvp;

out vec4 o_Color;
out vec2 o_TexCoord;
void main(void)
{
	gl_Position=vec4(in_Position,0.,1.)*u_mvp;
	o_TexCoord=in_TexCoord;
	o_Color =  vec4(1.,1.,1.,1.);
}