#version 450
in vec4 o_Color;
in vec2 o_TexCoord;

out vec4 out_Color;	
uniform sampler2D u_Texture;
void main(void)
{	
	out_Color = o_Color * texture(u_Texture, o_TexCoord);
}
