#include "matrix.hpp"
#include "types.hpp"
#include "utils.hpp"
#include <array>
#include <chrono>
#include <framebuffer.hpp>
#include <iostream>
#include <renderer.hpp>
#include <shader.hpp>
#include <texture.hpp>
#include <thread>
#include <vertexarray.hpp>
#include <video_init.hpp>
auto main(int argc, char **argv) -> int {
	using namespace std::chrono_literals;

	kczi::GlContext::Params window_params;
	window_params.window_name = "Kczi-gui-test";
	auto gl_context = std::move(kczi::init_viedo(window_params).get());
	kczi::Renderer::Params renderer_params;
	kczi::Renderer renderer(renderer_params);
	renderer.viewport.rect = { ty2(0u,0u), window_params.window_size};
	struct SimpleMVP {
		kczi::UniformLocation<kczi::Matrix> mvp;
		static constexpr auto has_texture() -> bool { return true; }
	};
	kczi::Shader<SimpleMVP> shader(kczi::read_file("assets/shader.frag"),
																 kczi::read_file("assets/shader.vert"));

	shader.get_uniform_locations(kczi::UniformPair("u_mvp", shader.shader_cache.mvp));

	using SimpleVert = kczi::Vertex<kczi::VAO::CoordType::XY, kczi::VAO::ColorType::NONE, kczi::VAO::UV::UV>;

	std::array<SimpleVert, 6> triangle{};

	triangle[0].m_data = {4.0f, 4.0f, 0.0f, 0.0f};
	triangle[1].m_data = {4.0f, -4.0f, 0.0f, 1.0f};
	triangle[2].m_data = {-4.0f, -4.0f, 1.0f, 1.0f};
	triangle[3].m_data = {-4.0f, 4.0f, 1.0f, 0.0f};
	triangle[4].m_data = {4.0f, 4.0f, 0.0f, 0.0f};
	triangle[5].m_data = {-4.0f, -4.0f, 1.0f, 1.0f};

	kczi::Texture texture{};
	texture.load("assets/photo.jpg");
	kczi::VertexArray<SimpleVert> vao((std::span<SimpleVert>(triangle)));
	shader.bind();
	kczi::Framebuffer framebuffer(renderer);
	framebuffer.create({200u, 200u});

	float time = 1;
	while (true) {
		renderer.clear();
		framebuffer.clear();
		framebuffer.bind();
		framebuffer.fb_viewport.bind();
		texture.bind();
		shader.set_uniform(shader.shader_cache.mvp, kczi::Matrix(ty2(10.0f, 10.0f), ty2(-10.0f, -10.0f)));
		vao.draw();
		kczi::Framebuffer::unbind();
		renderer.viewport.bind();
		framebuffer.texture.bind();
		shader.set_uniform(shader.shader_cache.mvp, kczi::Matrix(ty2(10.0f, 10.0f), ty2(-10.0f, -10.0f)));
	vao.draw();

		time++;

		std::this_thread::sleep_for(10ms);
		gl_context.swap();
	}
	return 0;
}